//
//  ShuttlePassTests.swift
//  ShuttlePassTests
//
//  Created by Sebastien Peek on 11/21/17.
//  Copyright © 2017 laudeon. All rights reserved.
//

import XCTest

@testable import ShuttlePass

class ShuttlePassTests: XCTestCase {
    
    var passes:[TripTemplate] = []
    
    override func setUp() {
        super.setUp()
        
        ShuttlePassesManager.getAvailablePasses { (success, response) in
            XCTAssertTrue(success)
            guard let passes = response as? [TripTemplate] else { return XCTAssertThrowsError("Passes not array of TripTemplate")}
            self.passes = passes
        }
        
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_manager_jsonParsesCorrectly() {
        XCTAssertNoThrow(try ShuttlePassesManager.jsonFor("shuttle_passes"))
    }
    
    func test_manager_jsonFileNotFoundError() {
        XCTAssertThrowsError(try ShuttlePassesManager.jsonFor("does_not_exist"))
    }
    
    func test_availablePasses_templateCreated() {
        XCTAssertNotNil(self.passes)
        
        if let firstTemplate = self.passes.first {
            XCTAssertNotNil(firstTemplate)
            XCTAssertTrue(firstTemplate.name == "Laruel Hall Pass")
        }
        
    }
    
    func test_googleApiRequest() {
        XCTAssertNotNil(self.passes)
        if let firstTemplate = self.passes.first {
            XCTAssertNotNil(firstTemplate.trip)
            if let trip = firstTemplate.trip {
                
                let routeExpectation = expectation(description: "Get route for waypoints")
                ShuttlePassesManager.getRouteForWaypoints(trip.waypoints!, complete: { (success, response) in
                    if success {
                        routeExpectation.fulfill()
                    } else {
                        XCTFail()
                    }
                })
                
                waitForExpectations(timeout: 2.0, handler: nil)
                
            }
            
        }
        
    }
    
}
