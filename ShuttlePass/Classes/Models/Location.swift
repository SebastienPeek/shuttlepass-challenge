//
//  Location.swift
//  ShuttlePass
//
//  Created by Sebastien Peek on 11/24/17.
//  Copyright © 2017 laudeon. All rights reserved.
//

import UIKit

class Location: NSObject {

    var latitude: Double?
    var longitude: Double?
    
    var name: String?
    
    init(json: [String: Any]) {
        
        self.latitude = json["latitude"] as? Double
        self.longitude = json["longitude"] as? Double
        
        self.name = json["name"] as? String
        
    }
    
}
