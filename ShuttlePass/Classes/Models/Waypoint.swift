//
//  Waypoint.swift
//  ShuttlePass
//
//  Created by Sebastien Peek on 11/21/17.
//  Copyright © 2017 laudeon. All rights reserved.
//

import UIKit

public enum WayPointType : NSInteger {
    case pickup = 0
    case dropoff = 1
}

class Waypoint: NSObject {

    var type: WayPointType?
    var location: Location?
    var passengers: [User]!
    
    init(json: [String: Any]) {
        
        self.type = WayPointType(rawValue: json["type"] as! Int)
        self.location = Location(json: json["location"] as! [String: Any])
        self.passengers = Users(usersJson: json["passengers"] as! [[String: Any]]).users
        
    }
    
}

open class Waypoints {
    var waypoints = Array<Waypoint>()
    
    required public init(waypointsJson: [[String: Any]]) {
        for waypoint:[String: Any] in waypointsJson {
            waypoints.append(Waypoint(json: waypoint))
        }
    }
}
