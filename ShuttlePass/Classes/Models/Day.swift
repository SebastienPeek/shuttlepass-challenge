//
//  Day.swift
//  ShuttlePass
//
//  Created by Sebastien Peek on 11/22/17.
//  Copyright © 2017 laudeon. All rights reserved.
//

import UIKit

class Day: NSObject {
    
    var title: String?
    
    init(json: [String: Any]) {
        self.title = json["title"] as? String
    }
    
}

open class Days {
    var days = Array<Day>()
    
    required public init(daysJson: [[String: Any]]) {
        for day:[String: Any] in daysJson {
            days.append(Day(json: day))
        }
    }
}
