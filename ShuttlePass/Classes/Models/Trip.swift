//
//  Trip.swift
//  ShuttlePass
//
//  Created by Sebastien Peek on 11/21/17.
//  Copyright © 2017 laudeon. All rights reserved.
//

import UIKit

class Trip: NSObject {

    var start: String?
    var end: String?
    
    var waypoints: [Waypoint]?
    
    init(json: [String: Any]) {
        
        self.start = json["start"] as? String
        self.end = json["end"] as? String
        self.waypoints = Waypoints(waypointsJson: json["waypoints"] as! [[String: Any]]).waypoints
        
    }
    
}
