//
//  User.swift
//  ShuttlePass
//
//  Created by Sebastien Peek on 11/21/17.
//  Copyright © 2017 laudeon. All rights reserved.
//

import UIKit

public enum UserType : NSInteger {
    case passenger = 0
    case organizer = 1
}

class User: NSObject {
    
    var type: UserType!

    var name: String?
    var age: Int?
    
    init(json: [String: Any]) {
        
        self.name = json["name"] as? String
        self.age = json["age"] as? Int
        
    }

}

open class Users {
    var users = Array<User>()
    
    required public init(usersJson: [[String: Any]]) {
        for user:[String: Any] in usersJson {
            users.append(User(json: user))
        }
    }
}
