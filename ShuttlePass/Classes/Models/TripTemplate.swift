//
//  TripTemplate.swift
//  ShuttlePass
//
//  Created by Sebastien Peek on 11/21/17.
//  Copyright © 2017 laudeon. All rights reserved.
//

import UIKit

class TripTemplate: NSObject {

    var id: String?
    var name: String?
    var index: Int?
    var start: Date?
    var end: Date?
    
    var days: [Day]?
    var trip: Trip?
    
    init(json: [String: Any]) {
        
        self.id = json["id"] as? String
        self.name = json["name"] as? String
        self.index = json["index"] as? Int
        
        self.start = Date(timeIntervalSince1970: json["start"] as! Double)
        self.end = Date(timeIntervalSince1970: json["end"] as! Double)
        
        self.days = Days(daysJson: json["days"] as! [[String: Any]]).days
        self.trip = Trip(json: json["trip"] as! [String: Any])
        
    }
    
}

open class TripTemplates {
    var templates = Array<TripTemplate>()
    
    required public init(templatesJson: [[String: Any]]) {
        for template:[String: Any] in templatesJson {
            templates.append(TripTemplate(json: template))
        }
    }
}
