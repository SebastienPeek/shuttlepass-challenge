//
//  ShuttlePassesManager.swift
//  ShuttlePass
//
//  Created by Sebastien Peek on 11/22/17.
//  Copyright © 2017 laudeon. All rights reserved.
//

import UIKit
import Alamofire

enum jsonError: Error {
    case fileNotFound
    case fileFailedToParse
    case unhandledError
}

public typealias ShuttlePassManagerCompletionHandler = (Bool, Any) -> Void

class ShuttlePassesManager {
    
    internal static var manager: Alamofire.SessionManager = {
        let configuration = URLSessionConfiguration.default
        return Alamofire.SessionManager(configuration: configuration)
    }()

    class func getAvailablePasses(_ complete: @escaping ShuttlePassManagerCompletionHandler) {
        
        do {
            if let passes = try jsonFor("shuttle_passes") as? [String: Any] {
                let availablePasses = TripTemplates(templatesJson: passes["templates"] as! [[String: Any]]).templates
                complete(true, availablePasses)
            }
        } catch jsonError.fileNotFound {
            complete(false, jsonError.fileNotFound)
        } catch jsonError.fileFailedToParse {
            complete(false, jsonError.fileFailedToParse)
        } catch {
            complete(false, jsonError.unhandledError)
        }
        
    }
    
    class func getRouteForWaypoints(_ waypoints: [Waypoint], complete: @escaping ShuttlePassManagerCompletionHandler) {
        
        let placesApiKey = "AIzaSyBVFwWqg-SwsSeyQWK3yvBXV0Ygf6WtKz0"
        
        let pickup = waypoints.first!
        let originLat = pickup.location!.latitude!
        let originLon = pickup.location!.longitude!
        
        let origin = "\(originLat),\(originLon)"
        
        let dropoff = waypoints.last!
        let destinationLat = dropoff.location!.latitude!
        let destinationLon = dropoff.location!.longitude!
        
        let destination = "\(destinationLat),\(destinationLon)"
        
        let urlString = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=\(origin)&destinations=\(destination)&key=\(placesApiKey)"
        
        let url = URL(string: urlString)!
        
        self.manager.request(url)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                switch response.result {
                case .success(let jsonDictionary):
                    complete(true, jsonDictionary)
                    break
                case .failure(let error):
                    complete(false, error)
                    break
                }
        }
        
        
    }
    
    class func jsonFor(_ resource: String?) throws -> Any {
        
        do {
            if let file = Bundle.main.url(forResource: resource, withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                   return object
                } else if let object = json as? [Any] {
                    // json is an array
                    return object
                } else {
                    throw jsonError.fileFailedToParse
                }
            } else {
                throw jsonError.fileNotFound
            }
        } catch {
            throw error
        }
        
    }
    
}
