//
//  ScheduleBaseTableViewCell.swift
//  ShuttlePass
//
//  Created by Sebastien Peek on 11/22/17.
//  Copyright © 2017 laudeon. All rights reserved.
//

import UIKit

class ScheduleBaseTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var customImageView: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!

}
