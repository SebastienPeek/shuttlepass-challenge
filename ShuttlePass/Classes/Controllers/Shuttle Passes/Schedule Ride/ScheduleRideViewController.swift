//
//  ScheduleRideViewController.swift
//  ShuttlePass
//
//  Created by Sebastien Peek on 11/21/17.
//  Copyright © 2017 laudeon. All rights reserved.
//

import UIKit
import MapKit

class ScheduleRideViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var toggleButton: UIButton!

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerViewPickupLabel: UILabel!
    @IBOutlet weak var headerViewPickupTimeLabel: UILabel!
    @IBOutlet weak var headerViewDropoffLabel: UILabel!
    @IBOutlet weak var headerViewDropoffTimeLabel: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!
    
    fileprivate var tableArray:[[String: Any]] = []
    
    var tripTemplate: TripTemplate!
    
    let formatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = Calendar.current.timeZone
        dateFormatter.locale = Calendar.current.locale
        dateFormatter.dateFormat = "MMM dd"
        return dateFormatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.tableFooterView = UIView()
        
        self.titleLabel.text = "\(tripTemplate.name!) \(tripTemplate.index ?? 0)"
        
        if let trip = tripTemplate.trip {
            if let waypoints = trip.waypoints {
                if waypoints.first!.type == .pickup {
                    if let first = waypoints.first {
                        self.headerViewPickupLabel.text = first.location!.name
                    }
                }
                
                if waypoints.last!.type == .dropoff {
                    if let last = waypoints.last {
                        self.headerViewDropoffLabel.text = last.location!.name
                    }
                }
            }
            self.headerViewPickupTimeLabel.text = trip.start
            self.headerViewDropoffTimeLabel.text = "est. \(trip.end!)"
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupTableView()
        setupMapView()
        
    }

    @IBAction func dismissController() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func toggleAction() {
        
        if headerView.isHidden {
            headerView.isHidden = false
            toggleButton.setTitle("Map", for: .normal)
            view.bringSubview(toFront: headerView)
        } else {
            headerView.isHidden = true
            toggleButton.setTitle("Info", for: .normal)
            view.sendSubview(toBack: headerView)
        }
        
    }

}

extension ScheduleRideViewController: MKMapViewDelegate {
    
    func setupMapView() {
        
        mapView.delegate = self
        
        if let waypoints = tripTemplate.trip?.waypoints {
            for waypoint in waypoints {
                let waypointMarker = WaypointMarker(data: waypoint, trip: tripTemplate.trip!)
                mapView.addAnnotation(waypointMarker)
            }
        }
        
        mapView.showAnnotations(mapView.annotations, animated: true)
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard let annotation = annotation as? WaypointMarker else { return nil }
        
        let identifier = "marker"
        var view: MKAnnotationView
        
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            as? MKAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
        }
        
        let waypoint = annotation.waypoint
        
        let pinView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: 12))
        pinView.layer.cornerRadius = 6
        if waypoint.type == .pickup {
            pinView.backgroundColor = UIColor(red: 244/255, green: 126/255 , blue: 69/255 , alpha: 1)
        } else {
            pinView.backgroundColor = UIColor(red: 23/255, green: 177/255 , blue: 204/255 , alpha: 1)
        }
        
        let image = UIImage(view: pinView)
        view.image = image
        
        return view
    }

}

extension ScheduleRideViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        
        var dayStringArray:[String] = []
        for day in tripTemplate.days! {
            dayStringArray.append(day.title!)
        }
        
        var rangeString:String
        switch dayStringArray.count {
        case 2:
            rangeString = dayStringArray.joined(separator: " & ")
        case 3:
            rangeString = dayStringArray.joined(separator: ", ")
        default:
            rangeString = dayStringArray.first! + " - " + dayStringArray.last!
        }
        
        let startString = formatter.string(from: tripTemplate.start!)
        let endString = formatter.string(from: tripTemplate.end!)
        
        let defaultColor = UIColor(red: 95/255, green: 108/255 , blue: 110/255 , alpha: 1)
        let mangoTango = UIColor(red: 244/255, green: 126/255 , blue: 69/255 , alpha: 1)
        
        let repeatingDict:[String: Any] = ["title": "Repeats weekly on", "image_name": "repeat_icon", "content":"\(rangeString) from \(startString) - \(endString)", "content_color": defaultColor]
        tableArray.append(repeatingDict)
        
        let ridersDict:[String: Any] = ["title": "Your Riders", "image_name": "riders_icon", "content": "Select Riders", "content_color": mangoTango]
        tableArray.append(ridersDict)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "baseCell", for: indexPath) as! ScheduleBaseTableViewCell
        
        let data = tableArray[indexPath.row]
        cell.titleLabel.text = data["title"] as? String
        cell.contentLabel.text = data["content"] as? String
        
        let contentColor = data["content_color"] as! UIColor
        cell.contentLabel.textColor = contentColor
        
        if let imageName = data["image_name"] as? String {
            cell.customImageView.image = UIImage(named: imageName)
        }
        
        return cell
    }
    
}

class WaypointMarker: NSObject, MKAnnotation {
    let title: String?
    let coordinate: CLLocationCoordinate2D
    
    let waypoint: Waypoint
    
    init(data: Waypoint!, trip: Trip!) {
        
        self.waypoint = data
        
        if waypoint.type == .pickup {
            self.title = trip.start
        } else {
            self.title = "est. \(trip.end!)"
        }
        
        self.coordinate = CLLocationCoordinate2D(latitude: waypoint.location!.latitude!, longitude: waypoint.location!.longitude!)
        
        super.init()
    }
    
}


// Helper function for converting UIView to UIImageView - https://stackoverflow.com/a/33644345
extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in:UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: image!.cgImage!)
    }
}
