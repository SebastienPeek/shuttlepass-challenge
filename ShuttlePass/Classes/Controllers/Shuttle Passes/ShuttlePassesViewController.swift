//
//  ShuttlePassesViewController.swift
//  ShuttlePass
//
//  Created by Sebastien Peek on 11/22/17.
//  Copyright © 2017 laudeon. All rights reserved.
//

import UIKit

class ShuttlePassesViewController: UIViewController {

    fileprivate var passes:[TripTemplate] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    let formatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = Calendar.current.timeZone
        dateFormatter.locale = Calendar.current.locale
        dateFormatter.dateFormat = "MMM dd"
        return dateFormatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.tableFooterView = UIView()
        
        ShuttlePassesManager.getAvailablePasses { (success, passes) in
            if success {
                if let passes = passes as? [TripTemplate] {
                    self.passes = passes
                }
            } else {
                // TODO: Add in error handling here, this would be used for network etc
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension ShuttlePassesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 91
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return passes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "shuttlePassCell", for: indexPath) as! ShuttlePassTableViewCell
        cell.dateFormatter = formatter
        cell.setupCell(data: passes[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showShuttleDetails" {
            if let cell = sender as? ShuttlePassTableViewCell {
                let destinationController = segue.destination as! ScheduleRideViewController
                destinationController.tripTemplate = cell.tripTemplate
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
