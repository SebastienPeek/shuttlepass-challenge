//
//  ShuttlePassTableViewCell.swift
//  ShuttlePass
//
//  Created by Sebastien Peek on 11/22/17.
//  Copyright © 2017 laudeon. All rights reserved.
//

import UIKit

class ShuttlePassTableViewCell: UITableViewCell {
    
    @IBOutlet weak var indexLabel: UILabel!
    
    @IBOutlet weak var rangeLabel: UILabel!
    @IBOutlet weak var pickupTimeLabel: UILabel!
    
    @IBOutlet weak var pickupLabel: UILabel!
    @IBOutlet weak var dropoffLabel: UILabel!
    
    var tripTemplate:TripTemplate!
    var passIndex: Int!
    var dateFormatter: DateFormatter!
    
    func setupCell(data: TripTemplate) {
        self.tripTemplate = data
        
        self.indexLabel.text = "\(tripTemplate.index ?? 0)"
        
        var dayStringArray:[String] = []
        for day in tripTemplate.days! {
            dayStringArray.append(day.title!)
        }
        
        var rangeString:String
        switch dayStringArray.count {
        case 2:
            rangeString = dayStringArray.joined(separator: " & ")
        case 3:
            rangeString = dayStringArray.joined(separator: ", ")
        default:
            rangeString = dayStringArray.first! + " - " + dayStringArray.last!
        }
        
        let startString = dateFormatter.string(from: tripTemplate.start!)
        let endString = dateFormatter.string(from: tripTemplate.end!)
        self.rangeLabel.text = "\(rangeString) from \(startString) - \(endString)"
        
        if let trip = tripTemplate.trip {
            if let waypoints = trip.waypoints {
                if waypoints.first!.type == .pickup {
                    if let first = waypoints.first {
                        self.pickupLabel.text = first.location!.name
                    }
                }
                
                if waypoints.last!.type == .dropoff {
                    if let last = waypoints.last {
                        self.dropoffLabel.text = last.location!.name
                    }
                }
            }
            self.pickupTimeLabel.text = trip.start
        }
        
    }

}
